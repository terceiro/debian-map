all: map.jpg

map.jpg: config developers.coords
	xplanet \
		-config config \
		-projection rectangular \
		-geometry 1500x900 \
		-num_times 1 \
		-output $@ \

developers.coords:
	wget -O $@ https://www.debian.org/devel/developers.coords

clean:
	$(RM) map.jpg developers.coords

reload:
	$(MAKE) clean
	$(MAKE)
